global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

exit: 
    mov rax, 60
    mov rdi, 0
    syscall
    ret 
stdout:
    mov rax, 1
    mov rdi, 1
    syscall
    ret
stderr:
    mov rax, 1
    mov rdi, 2
    syscall
    ret

string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
        je .exit
        inc rax
        jmp short .loop
    .exit:
        ret

print_string:
    xor rax, rax   
    mov rsi, rdi                
    call string_length          
    mov rdx, rax
    call stdout
    ret

print_newline:
    mov rdi, 10
    call print_char
    ret

print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1 
    mov rdi, 1
    mov rdx, 1
    syscall
    add rsp, 8
    ret

print_uint:
    push rbx 
    push rbp
    mov rbp, rsp
    mov rax, rdi
    mov rbx, 10
    dec rsp
    mov byte [rsp], 0
    .loop:
        xor rdx, rdx
        div rbx
        add rdx, 48
        dec rsp
        mov [rsp], dl
        test rax, rax
        jz .write_number
        jmp short .loop
    .write_number:
        mov rdi, rsp
        call print_string
        mov rsp, rbp
        pop rbp
        pop rbx
        ret

print_int:
    cmp rdi, 0
    jl .lessZero
    call print_uint
    ret
    .lessZero:
        push rdi
        mov rdi,'-'
        call print_char
        pop rdi
        neg rdi
        call print_uint
        ret

string_equals:
        xor     rax, rax
        push    r8
        push    r9
        .loop:
            mov     r8b, byte[rsi]
            mov     r9b, byte[rdi]
            inc     rsi
            inc     rdi
            cmp     r8b, r9b
            jne     .exit
            cmp     r9b, 0
            jne     .loop
            inc     rax
        .exit:
            pop     r9
            pop     r8
            ret

read_char:
    dec rsp
    mov rax, 0
    mov rdi, 0
    mov rdx, 1
    mov rsi, rsp
    syscall
    test rax, rax
    je .exit
    xor rax, rax
    mov al, [rsp]
    .exit:
        inc rsp
        ret

read_word:
        xor rcx, rcx
    .loop:
        push rdi
        push rsi
        push rcx
        call read_char
        pop rcx
        pop rsi
        pop rdi
        cmp rax, 0x20
        je .whitespace
        cmp rax, 0x9
        je .whitespace
        cmp rax, 0xA
        je .whitespace
        cmp rax, 0
        je .end
        mov [rdi + rcx], rax
        inc rcx
        cmp rcx, rsi 
        jge .err
        jmp .loop
    .whitespace:
        cmp rcx, 0
        je .loop
        jmp .end
    .err:
        xor rax, rax
        xor rdx, rdx
        ret
    .end:
        xor rax, rax
        mov [rdi + rcx], rax
        mov rax, rdi
        mov rdx, rcx
        ret


parse_uint:
    xor     rax, rax
    mov     r9, 10
    xor     rax, rax
    xor     rcx, rcx
    xor     rdx, rdx
    xor     rsi, rsi
    .parse_char:
        mov     sil, [rdi + rcx]
        cmp     sil, 0x30
        jl      .return
        cmp     sil, 0x39
        jg      .return
        inc     rcx
        sub     sil, 0x30
        mul     r9
        add     rax, rsi
        jmp     .parse_char
    .return:
        mov     rdx, rcx
        ret

parse_int:
    xor rax, rax
    cmp byte[rdi], 0x2d
    je .neg_parse
    call parse_uint
    ret
    .neg_parse:
        inc rdi
        call parse_uint
        cmp     rdx, 0
        je      .return
        neg     rax
        inc     rdx
    .return:
        ret

string_copy:
    xor rcx, rcx
    .loop:
        cmp rcx, rdx  
        je .of  
        mov r8, [rdi + rcx] 
        mov [rsi, rcx], r8 
        cmp r8, 0
        je .ok  
        inc rcx
        jmp .loop
    .of:
        xor rax, rax
        jmp .exit
    .ok:
        mov rax, rcx
    .exit:
        ret
