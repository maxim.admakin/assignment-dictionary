extern read_word
extern exit
extern print_string
extern find_word
extern print_newline
extern string_length
global _start

%include "words.inc"

section .data
    reading_error: db "Reading error", 0
	not_found: db "Key not found", 0

section .text
_start:
	mov rsi, 256
	sub rsp, 256
	mov rdi, rsp
	call read_word
	test rax,rax
	je .reading_error
	mov rdi,rax
	mov rsi, p
	call find_word
	test rax,rax
	je .bad_exit
	add rax, 8
	mov rdi, rax
	mov r8, rax
	call string_length
	add r8, rax
	inc r8
	mov rdi, r8
	mov r9, 1
	.exit:
		call print_string
		call print_newline 
		call exit
	.reading_error:
		mov rdi, reading_error
		jmp .exit_err
	.bad_exit:
		mov rdi, not_found
		jmp .exit_err
	.exit_err:
		call print_string
		call print_newline 
		mov rdi, 1
		call exit
	
