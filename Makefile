asm= nasm
CFLAGS=-felf64

main: main.asm colon.inc words.inc lib.asm dict.asm
	$(asm) $(CFLAGS)  lib.asm
	$(asm) $(CFLAGS) dict.asm
	$(asm) $(CFLAGS) main.asm
	ld -o main dict.o lib.o  main.o
	
clean:
	find . -type f -name '*.o' -delete
	rm -f main
	