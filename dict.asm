extern string_equals
global find_word

section .text
find_word:
	mov r8, rdi
	.loop:
		mov r9,rsi
		test rsi, rsi
		je .bad_exit
        mov rdi, r8
		add rsi,8
		call string_equals
		mov rsi, r9
		test rax, rax
		jnz .found
		mov rsi, [rsi]
		jmp .loop
	.found:
		mov rax, rsi
		ret
	.bad_exit:
		mov rax,0
		ret
